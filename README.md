#Welcome to the ReVault. 

ReVault is a system for secure cryptographic payments via cryptocurrencies, like BitCoin. It is composed of two parts - a static web application and a mobile application. 

The web application, named **ReVault Generator** is used to generate transactions. These transactions are then signed with private keys stored only on the mobile app, called **ReVault Wallet**, and then sent back to host machine to be transmitted to the blockchain. 

The beauty of the system lies in the ability for a user to send secure Bitcoin transactions, even on compromised machines. 

##Building
The app is built and served using grunt, a constituent component of Yeoman. 

To serve, execute ```grunt serve```. 

To build into a static site, execute ```grunt build```

##License
MIT

