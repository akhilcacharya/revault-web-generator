'use strict';

/**
 * @ngdoc overview
 * @name treasureApp
 * @description
 * # treasureApp
 *
 * Main module of the application.
 */
angular
  .module('treasureApp', [
    'ngCookies',
    'ngResource',
    'ngSanitize',
    'ngTouch'
  ]);

