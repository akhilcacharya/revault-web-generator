window.UIHandlers = {
	
	onType: function($scope){
		$scope.getAddressData(); 
        $scope.generateTransaction(); 
	}, 
	
	convertToBTC:  function(sat){
        var cfactor = 1e8;
        return Number(sat)/cfactor; 
    }, 
    
	convertToSAT : function(btc){
		var cfactor = 1e8
		return btc * cfactor; 
	}, 

	getChange : function(amount, inputs){ 
		var total = 0; 

		for(var i = 0; i < inputs.length; i++){
			total += inputs[i].value; 
		}

		console.log("total " + total + " amount " + amount);
		return total - amount;
	}, 


	getUnspents : function(amount, unspents){
		var total = 0; 
		var resultUnspents = []; 
		unspents.forEach(function(unspent){
			if(total < amount){
				total += unspent.value;
				resultUnspents.push(unspent); 
			}
		}); 
		return resultUnspents; 
	}, 

	calculateTxFee : function($scope, tx){
		var ins = tx.ins.length; 
		var outs = tx.outs.length; 
		var size = ins * 180 + outs * 34 + 10; 
		var upperLimit = size + ins; 
		var lowerLimit = size - ins;
		upperLimit = Math.ceil(upperLimit/1000); 
		var fee = upperLimit * 10000; 
		$scope.txData.transactionFee = fee; 
		return Number(fee); 
	}, 


	checkIfValidated : function($scope){
		$scope.txData.isValidated = ($scope.txData.sendAddress !== "" && $scope.txData.transactionAmount !== "" && !isNaN($scope.txData.transactionAmount) && $scope.txData.receiveAddress !== "") && ($scope.txData.alert === '');       
	}, 

	//Runs after transaction data has been validated 
	generateQRCode : function($scope, height, width){        
		var serialized = $scope.txData.tx.toHex().toString(); 
		//Persist Address
		Persist.save("address", $scope.txData.sendAddress); 

		var endpoint = "https://chart.googleapis.com/chart?chs=" + height + "x" + width + "&cht=qr&chl=" + encodeURI(serialized); 


		$scope.txData.qrCode = endpoint; 
		$scope.onload();
	}, 

	pushTx : function($scope){
		var form = new FormData(); 
		form.append("tx", $scope.txData.signedTransaction); 

		var xhrForm = new XMLHttpRequest(); 
		xhrForm.open('POST', endpoint); 
		xhrForm.send(form); 

		console.log("Sent!"); 
	}, 
	
	
	
	
	
}