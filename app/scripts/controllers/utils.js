window.Utils = {
	crypto: {
		reverseHash: function(hex){
			var bytes= []; 
			// Convert a hex string to a byte array
			for (var c = 0; c < hex.length; c += 2){
				bytes.push(parseInt(hex.substr(c, 2), 16));
			}
			return bytes.reverse();  
		}, 
		
		getHash: function(bytes){
			for (var hex = [], i = 0; i < bytes.length; i++) {
				hex.push((bytes[i] >>> 4).toString(16));
				hex.push((bytes[i] & 0xF).toString(16));
			}
			return hex.join("");
		}, 
	},
	
	error: {
		isNotDust: function(amt){
			return amt > 5430; 
		}
	}
}