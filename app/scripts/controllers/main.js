'use strict';

/**
 * @ngdoc function
 * @name treasureApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the treasureApp
 */
angular.module('treasureApp')
  .controller('MainCtrl', function ($scope, $http) {
           
    var Persist = window.Persist; 
	var utils = window.Utils; 
	
	  
    //Expose variables to page
    $scope.txData = {
        sendAddress: Persist.get("address"), 
        receiveAddress: "", 
        transactionAmount: 0,
        transactionAmountBTC: 0, 
        isValidated: false, 
        signedTransaction: [], 
        qrCode : "", 
        unspentValue: 0, 
        transactionFee: "0", 
        tx: {}, 
        alert: '', 
    }; 
       
      
    $scope.onload = function(){
      window.startQR($("#reader"), function(data){
          var chunk = String(data); 
          $scope.txData.signedTransaction = data;   
      },
      function(error){
         //show read errors 
          //console.log("Error: " + error); 
      }, function(videoError){
         //the video stream could be opened
      });         
    }
    
    $scope.generateTransaction = function(){
        var tx = new Bitcoin.Transaction();
        
        var blockchainEndpoint = "https://blockchain.info/unspent?active=" + $scope.txData.sendAddress + "&cors=true"; 
             
        $http.get(blockchainEndpoint).success(function(data, status, headers, config){
             //TODO account for more than one output
             //var prevHash = data.unspent_outputs[0].tx_hash; 
             //var outputNumber = data.unspent_outputs[0].tx_output_n;            
             //TODO: Display on FORM
                      
            var inputs = $scope.getUnspents($scope.txData.transactionAmount, data.unspent_outputs); 
           
            //add inputs to transaction
            inputs.forEach(function(input) { 
                //Reverse tx_hash
                var bytes = utils.crypto.reverseHash(input.tx_hash); 
                var hash = utils.crypto.getHash(bytes); 
                tx.addInput(hash, input.tx_output_n);
            });
            
            var fee = $scope.calculateTxFee(tx);  
            
			//check if $scope.txData.transactionAmount + fee < total amount in wallet
            
            var change = $scope.getChange($scope.txData.transactionAmount, inputs); 
            
            if(utils.error.isNotDust(change)){
                tx.addOutput($scope.txData.sendAddress, change);  //calculate amount to send back    
            }
            
            
            if(!isNaN($scope.txData.transactionAmount)){
                var amt = Number($scope.txData.transactionAmount) || 0;        
                var unspent = Number($scope.txData.unspentValue);  
                var total = amt - fee;
                //Check if total is non-negative and if it is greater than the minimum transaction size (5430)
                if(amt >= fee && utils.error.isNotDust(amt + change)){
                    if((amt) <= unspent){
                        tx.addOutput($scope.txData.receiveAddress, (amt - fee));   
                        $scope.txData.alert = ""; 
                    }else{
                        //ERROR - not going to work! 
                        //Show Alert
                        $scope.txData.alert = "Insufficient funds in wallet"; 
                    } 
                }else{
                        $scope.txData.alert = "Transaction is too small!"
                }                   
            }else{
                $scope.txData.alert = "Not a valid amount"; 
            }
            
            $scope.txData.tx = tx;
            $scope.checkIfValidated(); 
            console.log(tx); 
        }).
        error(function(data, status, headers, config) {
            console.log("ERR"); 
            console.log(data);
        });

    }
     
    $scope.onType = function($scope){
		$scope.getAddressData(); 
        $scope.generateTransaction(); 
	}; 
    
    $scope.onSAT = function(){
        console.log("SAT change"); 
        $scope.txData.transactionAmountBTC = $scope.convertToBTC($scope.txData.transactionAmount); 
        $scope.onType(); 
    }
    
    $scope.onBTC = function(){
        console.log("SAT change"); 
        $scope.txData.transactionAmount = $scope.convertToSAT($scope.txData.transactionAmountBTC); 
        $scope.onType(); 
    }
    
    $scope.getAddressData = function(){
        var blockchainEndpoint = "https://blockchain.info/unspent?active=" + $scope.txData.sendAddress + "&cors=true"; 
        $http.get(blockchainEndpoint).success(function(data, status, headers, config){
            var val = 0; 
            data.unspent_outputs.forEach(function(unspent){
                val += unspent.value; 
            })
 
            console.log(val); 
            
            $scope.txData.unspentValue = val;  
        }).
        error(function(data, status, headers, config) {
            console.log("ERR"); 
            console.log(data); 
            $scope.txData.unspentValue = "No unspent value found :("
          // called asynchronously if an error occurs
          // or server returns response with an error status.
        }); 
    }
    
    $scope.convertToBTC = function(sat){
        var cfactor = 1e8;
        return Number(sat)/cfactor; 
    }
    
    $scope.convertToSAT = function(btc){
        var cfactor = 1e8
        return btc * cfactor; 
    }
    
    $scope.getChange = function(amount, inputs){ 
        var total = 0; 
        
        for(var i = 0; i < inputs.length; i++){
            total += inputs[i].value; 
        }
        
        console.log("total " + total + " amount " + amount);
        return total - amount;
    }
    
    
    $scope.getUnspents = function(amount, unspents){
        var total = 0; 
        var resultUnspents = []; 
        unspents.forEach(function(unspent){
            if(total < amount){
                 total += unspent.value;
                 resultUnspents.push(unspent); 
            }
        }); 
        return resultUnspents; 
    }
    
    $scope.calculateTxFee = function(tx){
        var ins = tx.ins.length; 
        var outs = tx.outs.length; 
        var size = ins * 180 + outs * 34 + 10; 
        var upperLimit = size + ins; 
        var lowerLimit = size - ins;
        upperLimit = Math.ceil(upperLimit/1000); 
        var fee = upperLimit * 10000; 
        $scope.txData.transactionFee = fee; 
        return Number(fee); 
    }
    
    
    $scope.checkIfValidated = function(){
        $scope.txData.isValidated = ($scope.txData.sendAddress !== "" && $scope.txData.transactionAmount !== "" && !isNaN($scope.txData.transactionAmount) && $scope.txData.receiveAddress !== "") && ($scope.txData.alert === '');       
    }
    
    //Runs after transaction data has been validated 
    $scope.generateQRCode = function(height, width){        
        var serialized = $scope.txData.tx.toHex().toString(); 
        //Persist Address
        Persist.save("address", $scope.txData.sendAddress); 
        
        var endpoint = "https://chart.googleapis.com/chart?chs=" + height + "x" + width + "&cht=qr&chl=" + encodeURI(serialized); 
        
        
        $scope.txData.qrCode = endpoint; 
        $scope.onload();
    }
    
    $scope.pushTx = function(){
        var form = new FormData(); 
        form.append("tx", $scope.txData.signedTransaction); 
        
        var xhrForm = new XMLHttpRequest(); 
        xhrForm.open('POST', endpoint); 
        xhrForm.send(form); 
        
        console.log("Sent!"); 
    }
});