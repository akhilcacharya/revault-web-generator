window.Persist = {
    save: function(key, obj){
        var contents = localStorage.getItem('global'); 
        
        if(contents === null){
            localStorage.setItem('global', JSON.stringify(global)); 
        }
        
        var global = JSON.parse(contents) || {}; 
        
        global[key] = obj; 
        
        console.log(global); 
        
        localStorage.setItem('global', JSON.stringify(global)); 
    }, 
    
    get: function(key){
        var global = localStorage.getItem('global');         
        if(global === null){
            return ""; 
        }else{
            global = JSON.parse(global); 
            return global[key] || ""; 
        }
    }
}; 